﻿namespace MysqlBinLog
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mysql_connect = new System.Windows.Forms.Button();
            this.mysql_clear_recode = new System.Windows.Forms.Button();
            this.mysql_show_recode = new System.Windows.Forms.Button();
            this.savetofile = new System.Windows.Forms.Button();
            this.mysql_server = new System.Windows.Forms.TextBox();
            this.mysql_user = new System.Windows.Forms.TextBox();
            this.mysql_password = new System.Windows.Forms.TextBox();
            this.mysql_charset = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.mysql_log = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.help = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "主机";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "用户名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "密码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "编码";
            // 
            // mysql_connect
            // 
            this.mysql_connect.Location = new System.Drawing.Point(578, 5);
            this.mysql_connect.Name = "mysql_connect";
            this.mysql_connect.Size = new System.Drawing.Size(60, 23);
            this.mysql_connect.TabIndex = 5;
            this.mysql_connect.Text = "连接";
            this.mysql_connect.UseVisualStyleBackColor = true;
            this.mysql_connect.Click += new System.EventHandler(this.mysql_connect_Click);
            // 
            // mysql_clear_recode
            // 
            this.mysql_clear_recode.Location = new System.Drawing.Point(15, 35);
            this.mysql_clear_recode.Name = "mysql_clear_recode";
            this.mysql_clear_recode.Size = new System.Drawing.Size(75, 23);
            this.mysql_clear_recode.TabIndex = 6;
            this.mysql_clear_recode.Text = "清空记录";
            this.mysql_clear_recode.UseVisualStyleBackColor = true;
            this.mysql_clear_recode.Click += new System.EventHandler(this.mysql_clear_recode_Click);
            // 
            // mysql_show_recode
            // 
            this.mysql_show_recode.Location = new System.Drawing.Point(96, 35);
            this.mysql_show_recode.Name = "mysql_show_recode";
            this.mysql_show_recode.Size = new System.Drawing.Size(75, 23);
            this.mysql_show_recode.TabIndex = 7;
            this.mysql_show_recode.Text = "查看记录";
            this.mysql_show_recode.UseVisualStyleBackColor = true;
            this.mysql_show_recode.Click += new System.EventHandler(this.mysql_show_recode_Click);
            // 
            // savetofile
            // 
            this.savetofile.Location = new System.Drawing.Point(177, 35);
            this.savetofile.Name = "savetofile";
            this.savetofile.Size = new System.Drawing.Size(75, 23);
            this.savetofile.TabIndex = 8;
            this.savetofile.Text = "保存到文件";
            this.savetofile.UseVisualStyleBackColor = true;
            this.savetofile.Click += new System.EventHandler(this.savetofile_Click);
            // 
            // mysql_server
            // 
            this.mysql_server.Location = new System.Drawing.Point(49, 8);
            this.mysql_server.Name = "mysql_server";
            this.mysql_server.Size = new System.Drawing.Size(95, 21);
            this.mysql_server.TabIndex = 9;
            this.mysql_server.Text = "localhost";
            // 
            // mysql_user
            // 
            this.mysql_user.Location = new System.Drawing.Point(197, 8);
            this.mysql_user.Name = "mysql_user";
            this.mysql_user.Size = new System.Drawing.Size(86, 21);
            this.mysql_user.TabIndex = 10;
            this.mysql_user.Text = "root";
            // 
            // mysql_password
            // 
            this.mysql_password.Location = new System.Drawing.Point(324, 8);
            this.mysql_password.Name = "mysql_password";
            this.mysql_password.PasswordChar = '*';
            this.mysql_password.Size = new System.Drawing.Size(100, 21);
            this.mysql_password.TabIndex = 11;
            // 
            // mysql_charset
            // 
            this.mysql_charset.FormattingEnabled = true;
            this.mysql_charset.Items.AddRange(new object[] {
            "UTF8",
            "GBK"});
            this.mysql_charset.Location = new System.Drawing.Point(465, 8);
            this.mysql_charset.Name = "mysql_charset";
            this.mysql_charset.Size = new System.Drawing.Size(46, 20);
            this.mysql_charset.TabIndex = 12;
            this.mysql_charset.Text = "GBK";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(524, 10);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(48, 16);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.Text = "滤空";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 464);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(650, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel1.Text = "就绪";
            // 
            // mysql_log
            // 
            this.mysql_log.Location = new System.Drawing.Point(2, 64);
            this.mysql_log.Name = "mysql_log";
            this.mysql_log.Size = new System.Drawing.Size(648, 397);
            this.mysql_log.TabIndex = 15;
            this.mysql_log.Text = "";
            this.mysql_log.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mysql_log_MouseClick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "rtf";
            this.saveFileDialog1.FileName = "binlog.rtf";
            this.saveFileDialog1.Filter = "写字板文件|*.rtf";
            // 
            // help
            // 
            this.help.Location = new System.Drawing.Point(258, 35);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(75, 23);
            this.help.TabIndex = 16;
            this.help.Text = "帮助";
            this.help.UseVisualStyleBackColor = true;
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "关于";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 486);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.help);
            this.Controls.Add(this.mysql_log);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.mysql_charset);
            this.Controls.Add(this.mysql_password);
            this.Controls.Add(this.mysql_user);
            this.Controls.Add(this.mysql_server);
            this.Controls.Add(this.savetofile);
            this.Controls.Add(this.mysql_show_recode);
            this.Controls.Add(this.mysql_clear_recode);
            this.Controls.Add(this.mysql_connect);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "MySQL二进制日志查看工具 - By 天智软件";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCtiState_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button mysql_connect;
        private System.Windows.Forms.Button mysql_clear_recode;
        private System.Windows.Forms.Button mysql_show_recode;
        private System.Windows.Forms.Button savetofile;
        private System.Windows.Forms.TextBox mysql_server;
        private System.Windows.Forms.TextBox mysql_user;
        private System.Windows.Forms.TextBox mysql_password;
        private System.Windows.Forms.ComboBox mysql_charset;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.RichTextBox mysql_log;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button help;
        private System.Windows.Forms.Button button1;
    }
}

