﻿using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MysqlBinLog
{
    public partial class Main : Form
    {
        private MySqlConnection db;
        private bool flag = false;

        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 测试Mysql连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mysql_connect_Click(object sender, EventArgs e)
        {
            string connectionString = "server=" + this.mysql_server.Text + ";user=" + this.mysql_user.Text + ";password=" + this.mysql_password.Text + ";";
            this.flag = true;
            this.db = new MySqlConnection(connectionString);
            try
            {
                this.db.Open();
            }
            catch (Exception exception)
            {
                this.toolStripStatusLabel1.Text = "连接失败 :(";
                MessageBox.Show("连接失败 :(\r\n\r\n" + exception.Message);
                this.db.Close();
                this.flag = false;
            }
            finally
            {
                if (this.flag)
                {
                    this.toolStripStatusLabel1.Text = "恭喜恭喜，连接成功 :)";
                    this.mysql_connect.Enabled = false;
                }
            }
        }

        /// <summary>
        /// 查看记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mysql_show_recode_Click(object sender, EventArgs e)
        {
            
            if (this.flag)
                {
                    string cmdText = "show master status";
                    object obj2 = new MySqlCommand(cmdText, this.db).ExecuteScalar();
                    new MySqlCommand("SET NAMES " + this.mysql_charset.Text, this.db).ExecuteNonQuery();
                    if (obj2 != null)
                    {
                        string str4 = obj2.ToString();
                        MySqlDataReader reader = new MySqlCommand("show binlog events in '" + str4 + "'", this.db).ExecuteReader();
                        this.mysql_log.Text = "";
                        while (reader.Read())
                        {
                            string input = Regex.Replace(reader[5].ToString(), @"use([^;]*?);\s+|COMMIT|BEGIN", "");
                            if (input != "")
                            {
                                if (this.checkBox1.Checked)
                                {
                                    input = Regex.Replace(Regex.Replace(Regex.Replace(input, "\t+", " "), "\r+", ""), "\n+", " ");
                                }
                                Color empty = Color.Empty;
                                if (input.ToUpper().Contains("INSERT"))
                                {
                                    empty = Color.Red;
                                }
                                else if (input.ToUpper().Contains("UPDATE"))
                                {
                                    empty = Color.Blue;
                                }
                                else if (input.ToUpper().Contains("DELETE"))
                                {
                                    empty = Color.Gray;
                                }
                                else if (input.ToUpper().Contains("REPLACE"))
                                {
                                    empty = Color.Green;
                                }
                                int textLength = this.mysql_log.TextLength;
                                this.mysql_log.AppendText(input + "\r\n");
                                this.mysql_log.Select(textLength, input.Length);
                                this.mysql_log.SelectionColor = empty;
                            }
                        }
                        reader.Close();
                        this.toolStripStatusLabel1.Text = "恭喜恭喜，数据载入成功 :)";
                    }
                    else
                    {
                        this.toolStripStatusLabel1.Text = "暂无记录";
                        MessageBox.Show("是检查您的MySQL配置文件，确认是否启用了Binlog！");
                    }
                //this.db.Close();
                }
            else {
                MessageBox.Show("请先连接数据库");
            }

        }

        /// <summary>
        /// 清空记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mysql_clear_recode_Click(object sender, EventArgs e)
        {
                if (this.flag)
                {
                    string cmdText = "reset master";
                    new MySqlCommand(cmdText, this.db).ExecuteNonQuery();
                    this.mysql_log.Text = "";
                    this.toolStripStatusLabel1.Text = "恭喜恭喜，清空完成 :)";
                }
                else
                {
                    MessageBox.Show("请先连接数据库");
                }
        }

        private void savetofile_Click(object sender, EventArgs e)
        {
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.mysql_log.SaveFile(this.saveFileDialog1.FileName);
            }

        }

        private void mysql_log_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Clicks == 1)
            {
                int charIndexFromPosition = this.mysql_log.GetCharIndexFromPosition(new Point(e.X, e.Y));
                this.toolStripStatusLabel1.Text = "当前行号：" + this.mysql_log.GetLineFromCharIndex(charIndexFromPosition).ToString();
            }
        }

        private void frmCtiState_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.ApplicationExitCall)
                return;

            DialogResult result = MessageBox.Show("是否退出软件?", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                e.Cancel = false;
                if (this.flag) {
                    this.flag = false;
                    this.db.Close();
                }
                Application.Exit();
                
            }  
        }

        private void help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("启用MySQL启用二进制日志BinLog方法，\r\n先停止MySQL服务器，然后修改My.ini，加入或修改以下配置\r\nserver-id = 1\r\nlog-bin = binlog\r\nlog-bin-index = binlog.index");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.tintsoft.com");
        }
    }
}
